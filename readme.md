# InfluxDB - Query Builder

[![codebeat badge](https://codebeat.co/badges/319d446e-ca2b-4482-9629-6b7a776dfbad)](https://codebeat.co/projects/github-com-gautierlevert-influxdb-querybuilder-develop)

[![Build Status](https://travis-ci.org/GautierLevert/influxdb-querybuilder.svg?branch=develop)](https://travis-ci.org/GautierLevert/influxdb-querybuilder)

[ ![Download](https://api.bintray.com/packages/gautierlevert/maven/influxdb-querybuilder/images/download.svg) ](https://bintray.com/gautierlevert/maven/influxdb-querybuilder/_latestVersion)

## How to use it

The query builder is usable in Kotlin or Java

Kotlin :

```kotlin
val statement = select {
        fields("water_level")

        from("h2o_feet")

        where(tag("location") isEqualTo "santa_monica")

        orderBy(time().desc())
}
```

Java :

```java
final Select statement = select()
        .fields("water_level")
        .from("h2o_feet")
        .where(tag("location").isEqualTo("santa_monica"))
        .build();
```

See unit tests for more details.
