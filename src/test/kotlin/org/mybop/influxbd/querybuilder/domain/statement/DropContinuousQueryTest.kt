package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Key
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On

class DropContinuousQueryTest {

    @Test
    fun example() {
        val statement = DropContinuousQuery(
                queryName = Name("myquery"),
                on = On(Name("mydb"))
        )

        assertThat(statement.toString()).isEqualTo("DROP CONTINUOUS QUERY \"myquery\" ON \"mydb\"")
    }
}
