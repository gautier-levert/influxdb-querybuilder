package org.mybop.influxbd.querybuilder.domain.clause

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class LimitTest {
    @Test
    fun example() {
        val clause = Limit(
                10,
                100
        )

        assertThat(clause.toString()).isEqualTo("LIMIT 10 OFFSET 100")
    }
}
