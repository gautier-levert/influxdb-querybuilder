package org.mybop.influxbd.querybuilder.domain.statement

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.Tag
import org.mybop.influxbd.querybuilder.domain.clause.FromMeasurement
import org.mybop.influxbd.querybuilder.domain.clause.Where
import org.mybop.influxbd.querybuilder.domain.clause.WithKeyEqualTo
import org.mybop.influxbd.querybuilder.domain.clause.WithKeyIn

class ShowTagValuesTest {
    @Test
    fun example() {
        // show all tag values across all measurements for the region tag
        val statement = ShowTagValues(
                WithKeyEqualTo(Tag("region"))
        )
        assertThat(statement.toString()).isEqualTo("SHOW TAG VALUES WITH KEY = \"region\"")
    }

    @Test
    fun example2() {
        // show tag values from the cpu measurement for the region tag
        val statement = ShowTagValues(
                WithKeyEqualTo(Tag("region")),
                from = FromMeasurement(Measurement(Name("cpu")))
        )
        assertThat(statement.toString()).isEqualTo("SHOW TAG VALUES FROM \"cpu\" WITH KEY = \"region\"")
    }

    @Test
    fun example3() {
        // show tag values from the cpu measurement for region & host tag keys where service = 'redis'
        val where: Where = mock {
            on { toString() } doReturn "WHERE \"service\" = 'redis'"
        }

        val statement = ShowTagValues(
                WithKeyIn(Tag("region"), Tag("host")),
                from = FromMeasurement(Measurement(Name("cpu"))),
                where = where
        )
        assertThat(statement.toString()).isEqualTo("SHOW TAG VALUES FROM \"cpu\" WITH KEY IN (\"region\", \"host\") WHERE \"service\" = 'redis'")
    }
}
