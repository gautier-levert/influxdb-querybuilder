package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Key
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On

class ShowFieldKeyCardinalityTest {
    @Test
    fun example() {
        // show estimated cardinality of the field key set of current database
        val statement = ShowFieldKeyCardinality()

        assertThat(statement.toString()).isEqualTo("SHOW FIELD KEY CARDINALITY")
    }

    @Test
    fun example2() {
        // show exact cardinality on field key set of specified database
        val statement = ShowFieldKeyCardinality(
                exact = true,
                on = On(Name("mydb"))
        )

        assertThat(statement.toString()).isEqualTo("SHOW FIELD KEY EXACT CARDINALITY ON \"mydb\"")
    }
}
