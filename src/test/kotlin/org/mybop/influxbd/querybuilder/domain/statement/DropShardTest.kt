package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class DropShardTest {

    @Test
    fun example() {
        val statement = DropShard(1)

        assertThat(statement.toString()).isEqualTo("DROP SHARD 1")
    }
}
