package org.mybop.influxbd.querybuilder.domain.statement

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.Where

class DeleteTest {

    @Test
    fun example() {
        val from: From = mock {
            on { toString() } doReturn "FROM \"cpu\""
        }

        val statement = Delete(
                from = from
        )
        assertThat(statement.toString()).isEqualTo("DELETE FROM \"cpu\"")
    }

    @Test
    fun example2() {
        val from: From = mock {
            on { toString() } doReturn "FROM \"cpu\""
        }

        val where: Where = mock {
            on { toString() } doReturn "WHERE time < '2000-01-01T00:00:00Z'"
        }

        val statement = Delete(
                from = from,
                where = where
        )
        assertThat(statement.toString()).isEqualTo("DELETE FROM \"cpu\" WHERE time < '2000-01-01T00:00:00Z'")
    }

    @Test
    fun example3() {
        val where: Where = mock {
            on { toString() } doReturn "WHERE time < '2000-01-01T00:00:00Z'"
        }

        val statement = Delete(
                where = where
        )
        assertThat(statement.toString()).isEqualTo("DELETE WHERE time < '2000-01-01T00:00:00Z'")
    }
}
