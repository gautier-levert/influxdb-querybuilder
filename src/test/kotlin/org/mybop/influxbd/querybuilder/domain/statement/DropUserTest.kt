package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name

class DropUserTest {

    @Test
    fun example() {
        val statement = DropUser(Name("jdoe"))

        assertThat(statement.toString()).isEqualTo("DROP USER \"jdoe\"")
    }
}
