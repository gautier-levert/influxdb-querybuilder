package org.mybop.influxbd.querybuilder.domain.statement

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.Where

class ShowMeasurementsTest {
    @Test
    fun example() {
        // show all measurements
        val statement = ShowMeasurements()

        assertThat(statement.toString()).isEqualTo("SHOW MEASUREMENTS")
    }

    @Test
    fun example2() {
        // show measurements where region tag = 'uswest' AND host tag = 'serverA'

        val where: Where = mock {
            on { toString() } doReturn "WHERE \"region\" = 'uswest' AND \"host\" = 'serverA'"
        }

        val statement = ShowMeasurements(
                where = where
        )

        assertThat(statement.toString()).isEqualTo("SHOW MEASUREMENTS WHERE \"region\" = 'uswest' AND \"host\" = 'serverA'")
    }

    @Test
    fun example3() {
        // show measurements with name 'h2o'

        val statement = ShowMeasurements(
                measurement = Measurement(Name("h2o"))
        )

        assertThat(statement.toString()).isEqualTo("SHOW MEASUREMENTS WITH MEASUREMENT = \"h2o\"")
    }
}
