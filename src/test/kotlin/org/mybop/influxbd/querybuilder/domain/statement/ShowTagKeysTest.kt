package org.mybop.influxbd.querybuilder.domain.statement

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.FromMeasurement
import org.mybop.influxbd.querybuilder.domain.clause.Where

class ShowTagKeysTest {
    @Test
    fun example() {
        // show all tag keys
        val statement = ShowTagKeys()
        assertThat(statement.toString()).isEqualTo("SHOW TAG KEYS")
    }

    @Test
    fun example2() {
        // show all tag keys from the cpu measurement
        val statement = ShowTagKeys(
                from = FromMeasurement(Measurement(Name("cpu")))
        )
        assertThat(statement.toString()).isEqualTo("SHOW TAG KEYS FROM \"cpu\"")
    }

    @Test
    fun example3() {
        // show all tag keys from the cpu measurement where the region key = 'uswest'
        val where: Where = mock {
            on { toString() } doReturn "WHERE \"region\" = 'uswest'"
        }

        val statement = ShowTagKeys(
                from = FromMeasurement(Measurement(Name("cpu"))),
                where = where
        )
        assertThat(statement.toString()).isEqualTo("SHOW TAG KEYS FROM \"cpu\" WHERE \"region\" = 'uswest'")
    }
}
