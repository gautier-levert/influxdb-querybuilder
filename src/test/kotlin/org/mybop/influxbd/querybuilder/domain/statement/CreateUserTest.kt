package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class CreateUserTest {

    @Test
    fun example() {
        // Create a normal database user.

        val statement = CreateUser(
                Name("jdoe"),
                StringValue("1337password")
        )

        assertThat(statement.toString()).isEqualTo("CREATE USER \"jdoe\" WITH PASSWORD '1337password'")
    }

    @Test
    fun example2() {
        // Create an admin user.

        val statement = CreateUser(
                Name("jdoe"),
                StringValue("1337password"),
                true
        )

        assertThat(statement.toString()).isEqualTo("CREATE USER \"jdoe\" WITH PASSWORD '1337password' WITH ALL PRIVILEGES")
    }
}
