package org.mybop.influxbd.querybuilder.domain.clause

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class SerieLimitTest {
    @Test
    fun example() {
        val clause = SerieLimit(
                10,
                100
        )

        assertThat(clause.toString()).isEqualTo("SLIMIT 10 SOFFSET 100")
    }
}
