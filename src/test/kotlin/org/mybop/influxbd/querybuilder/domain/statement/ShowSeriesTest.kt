package org.mybop.influxbd.querybuilder.domain.statement

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.FromMeasurement
import org.mybop.influxbd.querybuilder.domain.clause.Where

class ShowSeriesTest {
    @Test
    fun example() {
        val where: Where = mock {
            on { toString() } doReturn "WHERE cpu = 'cpu8'"
        }

        val statement = ShowSeries(
                from = FromMeasurement(Measurement(Name("cpu"), Name("autogen"), Name("telegraf"))),
                where = where
        )
        assertThat(statement.toString()).isEqualTo("SHOW SERIES FROM \"telegraf\".\"autogen\".\"cpu\" WHERE cpu = 'cpu8'")
    }
}
