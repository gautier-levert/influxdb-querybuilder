package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.DestinationMode
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class CreateSubscriptionTest {
    @Test
    fun example() {
        // Create a SUBSCRIPTION on database 'mydb' and retention policy 'autogen' that send data to 'example.com:9090' via UDP.
        val statement = CreateSubscription(
                subscriptionName = Name("sub0"),
                database = Name("mydb"),
                retentionPolicy = Name("autogen"),
                mode = DestinationMode.ALL,
                host = StringValue("udp://example.com:9090")
        )

        assertThat(statement.toString()).isEqualTo("CREATE SUBSCRIPTION \"sub0\" ON \"mydb\".\"autogen\" DESTINATIONS ALL 'udp://example.com:9090'")
    }

    @Test
    fun example2() {
        // Create a SUBSCRIPTION on database 'mydb' and retention policy 'autogen' that round robins the data to 'h1.example.com:9090' and 'h2.example.com:9090'.
        val statement = CreateSubscription(
                subscriptionName = Name("sub0"),
                database = Name("mydb"),
                retentionPolicy = Name("autogen"),
                mode = DestinationMode.ANY,
                host = StringValue("udp://h1.example.com:9090"),
                others = *arrayOf(StringValue("udp://h2.example.com:9090"))
        )

        assertThat(statement.toString()).isEqualTo("CREATE SUBSCRIPTION \"sub0\" ON \"mydb\".\"autogen\" DESTINATIONS ANY 'udp://h1.example.com:9090', 'udp://h2.example.com:9090'")
    }
}
