package org.mybop.influxbd.querybuilder.domain.clause

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name

class OnTest {
    @Test
    fun example() {
        val clause = On(Name("mydb"))

        assertThat(clause.toString()).isEqualTo("ON \"mydb\"")
    }
}
