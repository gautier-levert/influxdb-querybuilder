package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.Privilege
import org.mybop.influxbd.querybuilder.domain.clause.On

class RevokeTest {
    @Test
    fun example() {
        // revoke admin privileges from jdoe

        val statement = Revoke(Privilege.ALL, Name("jdoe"))

        assertThat(statement.toString()).isEqualTo("REVOKE ALL FROM \"jdoe\"")
    }

    @Test
    fun example2() {
        // revoke read privileges from jdoe on mydb

        val statement = Revoke(Privilege.READ, Name("jdoe"), On(Name("mydb")))

        assertThat(statement.toString()).isEqualTo("REVOKE READ ON \"mydb\" FROM \"jdoe\"")
    }
}
