package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On

class ShowMeasurementCardinalityTest {
    @Test
    fun example() {
        // show estimated cardinality of measurement set on current database
        val statement = ShowMeasurementCardinality()

        assertThat(statement.toString()).isEqualTo("SHOW MEASUREMENT CARDINALITY")
    }

    @Test
    fun example2() {
        // show exact cardinality of measurement set on specified database
        val statement = ShowMeasurementCardinality(
                exact = true,
                on = On(Name("mydb"))
        )

        assertThat(statement.toString()).isEqualTo("SHOW MEASUREMENT EXACT CARDINALITY ON \"mydb\"")
    }
}
