package org.mybop.influxbd.querybuilder.domain.clause

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Field
import org.mybop.influxbd.querybuilder.domain.condition.ValueCondition
import org.mybop.influxbd.querybuilder.domain.condition.ValueOperator
import org.mybop.influxbd.querybuilder.domain.value.FloatValue

class WhereTest {
    @Test
    fun example() {
        val clause = Where(
                ValueCondition(
                        Field("temp"),
                        ValueOperator.GREATER_THAN_OR_EQUAL_TO,
                        FloatValue(29.7f)
                )
        )

        assertThat(clause.toString()).isEqualTo("WHERE \"temp\" >= 29.7")
    }
}
