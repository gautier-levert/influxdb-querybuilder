package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.FromMeasurement

class ShowFieldKeysTest {
    @Test
    fun example() {
        // show field keys and field value data types from all measurements
        val statement = ShowFieldKeys()

        assertThat(statement.toString()).isEqualTo("SHOW FIELD KEYS")
    }

    @Test
    fun example2() {
        //show field keys and field value data types from specified measurement
        val statement = ShowFieldKeys(
                from = FromMeasurement(Measurement(Name("cpu")))
        )

        assertThat(statement.toString()).isEqualTo("SHOW FIELD KEYS FROM \"cpu\"")
    }
}
