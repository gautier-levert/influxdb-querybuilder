package org.mybop.influxbd.querybuilder.domain.statement

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

class CreateContinuousQueryTest {

    @Test
    fun example() {
        val select: Select = mock {
            on { toString() } doReturn "SELECT count(\"value\") INTO \"6_months\".\"events\" FROM \"events\" GROUP (10m)"
        }

        // selects from DEFAULT retention policy and writes into 6_months retention policy
        val statement = CreateContinuousQuery(
                queryName = Name("10m_event_count"),
                on = On(Name("db_name")),
                select = select
        )

        assertThat(statement.toString()).isEqualTo("CREATE CONTINUOUS QUERY \"10m_event_count\" ON \"db_name\" BEGIN SELECT count(\"value\") INTO \"6_months\".\"events\" FROM \"events\" GROUP (10m) END")
    }

    @Test
    fun example2() {
        val select: Select = mock {
            on { toString() } doReturn "SELECT mean(\"value\") INTO \"cpu_mean\" FROM \"cpu\" GROUP BY time(1m)"
        }

        // this customizes the resample interval so the interval is queried every 10s and intervals are resampled until 2m after their start time
        // when resample is used, at least one of "EVERY" or "FOR" must be used

        val statement = CreateContinuousQuery(
                queryName = Name("cpu_mean"),
                on = On(Name("db_name")),
                every = StringDuration("10s"),
                `for` = StringDuration("2m"),
                select = select
        )

        assertThat(statement.toString()).isEqualTo("CREATE CONTINUOUS QUERY \"cpu_mean\" ON \"db_name\" RESAMPLE EVERY 10s FOR 2m BEGIN SELECT mean(\"value\") INTO \"cpu_mean\" FROM \"cpu\" GROUP BY time(1m) END")
    }
}
