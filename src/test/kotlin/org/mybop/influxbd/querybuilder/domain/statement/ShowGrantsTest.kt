package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name

class ShowGrantsTest {
    @Test
    fun example() {
        // show grants for jdoe

        val statement = ShowGrants(Name("jdoe"))

        assertThat(statement.toString()).isEqualTo("SHOW GRANTS FOR \"jdoe\"")
    }
}
