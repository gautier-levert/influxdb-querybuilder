package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class KillQueryTest {
    @Test
    fun example() {
        val statement = KillQuery(123)

        assertThat(statement.toString()).isEqualTo("KILL QUERY 123")
    }
}
