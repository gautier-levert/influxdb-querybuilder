package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

class CreateRetentionPolicyTest {

    @Test
    fun example() {
        // Create a retention policy.
        val statement = CreateRetentionPolicy(
                policyName = Name("10m.events"),
                on = On(Name("somedb")),
                duration = StringDuration("60m"),
                replication = 2
        )

        assertThat(statement.toString()).isEqualTo("CREATE RETENTION POLICY \"10m.events\" ON \"somedb\" DURATION 60m REPLICATION 2")
    }

    @Test
    fun example2() {
        // Create a retention policy and set it as the DEFAULT.
        val statement = CreateRetentionPolicy(
                policyName = Name("10m.events"),
                on = On(Name("somedb")),
                duration = StringDuration("60m"),
                replication = 2,
                default = true
        )

        assertThat(statement.toString()).isEqualTo("CREATE RETENTION POLICY \"10m.events\" ON \"somedb\" DURATION 60m REPLICATION 2 DEFAULT")
    }

    @Test
    fun example3() {
        // Create a retention policy and set it as the DEFAULT.
        val statement = CreateRetentionPolicy(
                policyName = Name("10m.events"),
                on = On(Name("somedb")),
                duration = StringDuration("60m"),
                replication = 2,
                shardDuration = StringDuration("30m")
        )

        assertThat(statement.toString()).isEqualTo("CREATE RETENTION POLICY \"10m.events\" ON \"somedb\" DURATION 60m REPLICATION 2 SHARD DURATION 30m")
    }
}
