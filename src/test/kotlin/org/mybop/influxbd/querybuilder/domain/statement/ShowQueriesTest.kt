package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ShowQueriesTest {
    @Test
    fun example() {
        // show all currently-running queries
        val statement = ShowQueries()

        assertThat(statement.toString()).isEqualTo("SHOW QUERIES")
    }
}
