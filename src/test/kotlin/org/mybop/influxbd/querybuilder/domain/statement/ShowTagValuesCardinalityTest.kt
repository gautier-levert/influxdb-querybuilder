package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Key
import org.mybop.influxbd.querybuilder.domain.Tag
import org.mybop.influxbd.querybuilder.domain.clause.WithKeyEqualTo

class ShowTagValuesCardinalityTest {
    @Test
    fun example() {
        // show estimated tag key values cardinality for a specified tag key
        val statement = ShowTagValuesCardinality(
                WithKeyEqualTo(Tag("myTagKey"))
        )

        assertThat(statement.toString()).isEqualTo("SHOW TAG VALUES CARDINALITY WITH KEY = \"myTagKey\"")
    }

    @Test
    fun example2() {
        // show exact tag key values cardinality for a specified tag key
        val statement = ShowTagValuesCardinality(
                WithKeyEqualTo(Tag("myTagKey")),
                exact = true
        )

        assertThat(statement.toString()).isEqualTo("SHOW TAG VALUES EXACT CARDINALITY WITH KEY = \"myTagKey\"")
    }
}
