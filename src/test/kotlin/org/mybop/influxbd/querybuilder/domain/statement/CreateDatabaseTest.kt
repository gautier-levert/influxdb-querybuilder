package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

class CreateDatabaseTest {

    @Test
    fun example() {
        // Create a database called foo
        val statement = CreateDatabase(
                databaseName = Name("foo")
        )

        assertThat(statement.toString()).isEqualTo("CREATE DATABASE \"foo\"")
    }

    @Test
    fun example2() {
        // Create a database called bar with a new DEFAULT retention policy and specify the duration, replication, shard group duration, and name of that retention policy
        val statement = CreateDatabase(
                databaseName = Name("bar"),
                retentionPolicyDuration = StringDuration("1d"),
                retentionPolicyReplication = 1,
                retentionPolicyShardGroupDuration = StringDuration("30m"),
                retentionPolicyName = Name("myrp")
        )

        assertThat(statement.toString()).isEqualTo("CREATE DATABASE \"bar\" WITH DURATION 1d REPLICATION 1 SHARD DURATION 30m NAME \"myrp\"")
    }

    @Test
    fun example3() {
        // Create a database called mydb with a new DEFAULT retention policy and specify the name of that retention policy
        val statement = CreateDatabase(
                databaseName = Name("mydb"),
                retentionPolicyName = Name("myrp")
        )

        assertThat(statement.toString()).isEqualTo("CREATE DATABASE \"mydb\" WITH NAME \"myrp\"")
    }
}
