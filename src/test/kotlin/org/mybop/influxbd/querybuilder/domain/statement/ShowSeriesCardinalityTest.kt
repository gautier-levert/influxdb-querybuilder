package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On

class ShowSeriesCardinalityTest {
    @Test
    fun example() {
        // show estimated cardinality of the series on current database
        val statement = ShowSeriesCardinality()

        assertThat(statement.toString()).isEqualTo("SHOW SERIES CARDINALITY")
    }

    @Test
    fun example2() {
        // show estimated cardinality of the series on specified database
        val statement = ShowSeriesCardinality(
                on = On(Name("mydb"))
        )

        assertThat(statement.toString()).isEqualTo("SHOW SERIES CARDINALITY ON \"mydb\"")
    }

    @Test
    fun example3() {
        // show exact series cardinality
        val statement = ShowSeriesCardinality(
                exact = true
        )

        assertThat(statement.toString()).isEqualTo("SHOW SERIES EXACT CARDINALITY")
    }

    @Test
    fun example4() {
        // show series cardinality of the series on specified database
        val statement = ShowSeriesCardinality(
                exact = true,
                on = On(Name("mydb"))
        )

        assertThat(statement.toString()).isEqualTo("SHOW SERIES EXACT CARDINALITY ON \"mydb\"")
    }
}
