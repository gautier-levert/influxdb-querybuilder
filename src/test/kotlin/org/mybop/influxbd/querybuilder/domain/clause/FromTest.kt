package org.mybop.influxbd.querybuilder.domain.clause

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Name

class FromTest {
    @Test
    fun example() {
        val clause = FromMeasurement(
                Measurement(Name("m1")),
                Measurement(Name("m2")),
                Measurement(Name("m3"))
        )

        assertThat(clause.toString()).isEqualTo("FROM \"m1\", \"m2\", \"m3\"")
    }
}
