package org.mybop.influxbd.querybuilder.domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class MeasurementTest {

    @Test
    fun simpleName() {
        val measurement = Measurement(measurementName = Name("h2o_feet"))

        assertThat(measurement.toString()).isEqualTo("\"h2o_feet\"")
    }

    @Test
    fun fullyQualifiedName() {
        val measurement = Measurement(databaseName = Name("database"), retentionPolicyName = Name("retention"), measurementName = Name("h2o_feet"))

        assertThat(measurement.toString()).isEqualTo("\"database\".\"retention\".\"h2o_feet\"")
    }

    @Test
    fun noRetentionName() {
        val measurement = Measurement(databaseName = Name("database"), measurementName = Name("h2o_feet"))

        assertThat(measurement.toString()).isEqualTo("\"database\"..\"h2o_feet\"")
    }

    @Test
    fun restionAndName() {
        val measurement = Measurement(retentionPolicyName = Name("retention"), measurementName = Name("h2o_feet"))

        assertThat(measurement.toString()).isEqualTo("\"retention\".\"h2o_feet\"")
    }
}


