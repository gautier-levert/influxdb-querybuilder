package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.Name

class DropDatabaseTest {

    @Test
    fun example() {
        val statement = DropDatabase(
                database = Name("mydb")
        )

        assertThat(statement.toString()).isEqualTo("DROP DATABASE \"mydb\"")
    }
}
