package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ShowTagKeyCardinalityTest {
    @Test
    fun example() {
        // show estimated tag key cardinality
        val statement = ShowTagKeyCardinality()

        assertThat(statement.toString()).isEqualTo("SHOW TAG KEY CARDINALITY")
    }

    @Test
    fun example2() {
        // show exact tag key cardinality
        val statement = ShowTagKeyCardinality(
                true
        )

        assertThat(statement.toString()).isEqualTo("SHOW TAG KEY EXACT CARDINALITY")
    }
}
