package org.mybop.influxbd.querybuilder.domain.statement

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class ShowUsersTest {
    @Test
    fun example() {
        // show all users
        val statement = ShowUsers()
        assertThat(statement.toString()).isEqualTo("SHOW USERS")
    }
}
