package org.mybop.influxbd.querybuilder.domain.value

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class StringValueTest {
    @Test
    fun simpleValue() {
        val value = StringValue("value")
        assertThat(value.toString()).isEqualTo("'value'")
    }
}
