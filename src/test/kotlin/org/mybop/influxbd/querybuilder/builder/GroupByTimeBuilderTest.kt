package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class GroupByTimeBuilderTest {
    @Test
    fun example1() {
        val statement = select {
            fields(
                    count("water_level")
            )
            where(tag("location") isEqualTo "coyote_creek" and time().isAfter("2015-08-18T00:00:00Z") and time().isBefore("2015-08-18T00:30:00Z"))
            from("h2o_feet")
            groupBy(time("12m"))
        }

        assertThat(statement.toString()).isEqualTo("SELECT COUNT(\"water_level\") FROM \"h2o_feet\" WHERE \"location\" = 'coyote_creek' AND time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:30:00Z' GROUP BY time(12m)")
    }

    @Test
    fun example2() {
        val statement = select {
            fields(
                    count("water_level")
            )
            where(tag("location") isEqualTo "coyote_creek" and time().isAfter("2015-08-18T00:00:00Z") and time().isBefore("2015-08-18T00:30:00Z"))
            from("h2o_feet")
            groupBy(time("12m"), tag("location"))
        }

        assertThat(statement.toString()).isEqualTo("SELECT COUNT(\"water_level\") FROM \"h2o_feet\" WHERE \"location\" = 'coyote_creek' AND time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:30:00Z' GROUP BY time(12m), \"location\"")
    }
}
