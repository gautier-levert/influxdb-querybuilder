package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class SerieLimitBuilderTest {

    @Test
    fun example1() {
        val statement = select {
            fields(
                    "water_level"
            )

            from("h2o_feet")

            groupByAllTags()

            serieLimit(1)
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"water_level\" FROM \"h2o_feet\" GROUP BY * SLIMIT 1")
    }

    @Test
    fun example2() {
        val statement = select {
            fields(
                    mean("water_level")
            )

            from("h2o_feet")

            where(
                    time() isAfter "2015-08-18T00:00:00Z" and (
                            time() isBefore "2015-08-18T00:42:00Z"
                            )
            )

            groupBy(all(), time("12m"))

            serieLimit(1)
        }

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"water_level\") FROM \"h2o_feet\" WHERE time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:42:00Z' GROUP BY *, time(12m) SLIMIT 1")
    }

    @Test
    fun example3() {
        val statement = select {
            fields(
                    "water_level"
            )

            from("h2o_feet")

            groupByAllTags()

            limit(3)

            serieLimit(1)
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"water_level\" FROM \"h2o_feet\" GROUP BY * LIMIT 3 SLIMIT 1")
    }

    @Test
    fun example4() {
        val statement = select {
            fields(
                    mean("water_level")
            )

            from("h2o_feet")

            where(
                    time() isAfter "2015-08-18T00:00:00Z" and (
                            time() isBefore "2015-08-18T00:42:00Z"
                            )
            )

            groupBy(all(), time("12m"))

            limit(2)

            serieLimit(1)
        }

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"water_level\") FROM \"h2o_feet\" WHERE time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:42:00Z' GROUP BY *, time(12m) LIMIT 2 SLIMIT 1")
    }

    @Test
    fun example5() {
        val statement = select {
            fields(
                    "water_level"
            )

            from("h2o_feet")

            groupByAllTags()

            serieLimit(1)
            serieOffset(1)
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"water_level\" FROM \"h2o_feet\" GROUP BY * SLIMIT 1 SOFFSET 1")
    }

    @Test
    fun example6() {
        val statement = select {
            fields(
                    mean("water_level")
            )

            from("h2o_feet")

            where(
                    time() isAfter "2015-08-18T00:00:00Z" and (
                            time() isBefore "2015-08-18T00:42:00Z"
                            )
            )

            groupBy(all(), time("12m"))

            orderBy(time().desc())

            limit(2)
            offset(2)

            serieLimit(1)
            serieOffset(1)
        }

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"water_level\") FROM \"h2o_feet\" WHERE time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:42:00Z' GROUP BY *, time(12m) ORDER BY time DESC LIMIT 2 OFFSET 2 SLIMIT 1 SOFFSET 1")
    }
}
