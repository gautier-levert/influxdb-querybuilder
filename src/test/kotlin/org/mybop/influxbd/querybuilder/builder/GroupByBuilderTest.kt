package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class GroupByBuilderTest {
    @Test
    fun example1() {
        val statement = select {
            fields(
                    mean("water_level")
            )
            from("h2o_feet")
            groupBy("location")
        }

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"water_level\") FROM \"h2o_feet\" GROUP BY \"location\"")
    }

    @Test
    fun example2() {
        val statement = select {
            fields(
                    mean("index")
            )
            from("h2o_quality")
            groupBy("location", "randtag")
        }

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"index\") FROM \"h2o_quality\" GROUP BY \"location\", \"randtag\"")
    }

    @Test
    fun example3() {
        val statement = select {
            fields(
                    mean("index")
            )
            from("h2o_quality")
            groupByAllTags()
        }

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"index\") FROM \"h2o_quality\" GROUP BY *")
    }
}
