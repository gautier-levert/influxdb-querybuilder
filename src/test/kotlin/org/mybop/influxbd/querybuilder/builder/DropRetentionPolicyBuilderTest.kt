package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class DropRetentionPolicyBuilderTest {

    @Test
    fun example() {
        // drop the retention policy named 1h.cpu from mydb
        val statement = dropRetentionPolicy {
            named("1h.cpu")
            on("mydb")
        }

        assertThat(statement.toString()).isEqualTo("DROP RETENTION POLICY \"1h.cpu\" ON \"mydb\"")
    }
}
