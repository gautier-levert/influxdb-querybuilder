package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class WhereBuilderTest {

    @Test
    fun specificFieldKeyValues() {
        //  Select data that have specific field key-values
        val statement = select {
            from("h2o_feet")
            where(
                    field("water_level") isGreaterThan 8
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\" WHERE \"water_level\" > 8")
    }

    @Test
    fun specificFieldKeyValues2() {
        // Select data that have a specific string field key-value
        val statement = select {
            from("h2o_feet")
            where(
                    field("level description") isEqualTo "below 3 feet"
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\" WHERE \"level description\" = 'below 3 feet'")
    }

    @Test
    fun specificFieldKeyValuesBasicArithmetic() {
        // Select data that have a specific field key-value and perform basic arithmetic
        val statement = select {
            from("h2o_feet")
            where(
                    field("water_level") + 2 isGreaterThan 11.9
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\" WHERE \"water_level\" + 2 > 11.9")
    }

    @Test
    fun specificFieldKeyAndTagValues() {
        // Select data that have specific field key-values and tag key-values
        val statement = select {
            fields("water_level")
            from("h2o_feet")
            where(
                    tag("location") isNotEqualTo "santa_monica"
                            and ((field("water_level") isLessThan -0.59) or (field("water_level") isGreaterThan 9.95))
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"water_level\" FROM \"h2o_feet\" WHERE \"location\" != 'santa_monica' AND (\"water_level\" < -0.59 OR \"water_level\" > 9.95)")
    }

    @Test
    fun specificTimestamp() {
        // Select data that have specific field key-values and tag key-values
        val statement = select {
            from("h2o_feet")
            where(
                    time() isStrictlyAfter (now() - "7d")
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\" WHERE time > now() - 7d")
    }
}
