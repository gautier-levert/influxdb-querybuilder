package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class SelectBuilderTest {

    @Test
    fun allFieldsFromSingleMeasurement() {
        //  Select all fields and tags from a single measurement
        val statement = select {
            allFields()
            from("h2o_feet")
        }

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\"")
    }

    @Test
    fun specificFields() {
        // Select specific tags and fields from a single measurement
        val statement = select {
            fields("level description", "location", "water_level")
            from("h2o_feet")
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"level description\", \"location\", \"water_level\" FROM \"h2o_feet\"")
    }

    @Test
    fun basicArithmetic() {
        // Select a specific field from a measurement and perform basic arithmetic
        val statement = select {
            fields(
                    (field("water_level") * 2) + 4
            )

            from("h2o_feet")
        }

        assertThat(statement.toString()).isEqualTo("SELECT (\"water_level\" * 2) + 4 FROM \"h2o_feet\"")
    }

    @Test
    fun moreThanOneMeasurement() {
        // Select all data from more than one measurement
        val statement = select {
            from("h2o_feet", "h2o_pH")
        }

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\", \"h2o_pH\"")
    }

    @Test
    fun fullyQualifiedMeasurement() {
        // Select all data from a fully qualified measurement
        val statement = select {
            from(measurement("NOAA_water_database", "autogen", "h2o_feet"))
        }

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"NOAA_water_database\".\"autogen\".\"h2o_feet\"")
    }

    @Test
    fun defaultMeasurement() {
        // Select all data from a measurement in a particular database
        val statement = select {
            from(measurementDefaultRetention("NOAA_water_database", "h2o_feet"))
        }

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"NOAA_water_database\"..\"h2o_feet\"")
    }

    @Test
    fun fieldsWithAliases() {
        val statement = select {
            fields(
                    time().alias("receivedAt"),
                    field("createdAt"),
                    field("contract"),
                    last("reportingPacket").alias("reportingPacket")
            )
            from(measurement("retention", "measurement"))
            groupBy("contract")
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"time\" AS \"receivedAt\", \"createdAt\", \"contract\", LAST(\"reportingPacket\") AS \"reportingPacket\" FROM \"retention\".\"measurement\" GROUP BY \"contract\"")
    }
}
