package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class GroupByTimeAndFillBuilderTest {
    @Test
    fun example1() {
        val statement = select {
            fields(
                    max("water_level")
            )

            from("h2o_feet")

            where(
                    tag("location") isEqualTo "coyote_creek" and (time() isAfter "2015-09-18T16:00:00Z") and (time() isBefore "2015-09-18T16:42:00Z")
            )

            groupBy(
                    time("12m")
            )

            fill(100)
        }

        assertThat(statement.toString()).isEqualTo("SELECT MAX(\"water_level\") FROM \"h2o_feet\" WHERE \"location\" = 'coyote_creek' AND time >= '2015-09-18T16:00:00Z' AND time <= '2015-09-18T16:42:00Z' GROUP BY time(12m) fill(100)")
    }
}
