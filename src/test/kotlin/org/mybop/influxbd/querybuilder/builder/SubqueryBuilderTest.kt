package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Ignore
import org.junit.Test

class SubqueryBuilderTest {

    @Test
    fun example1() {
        val statement = select {
            fields(
                    sum("max")
            )

            from(select {
                fields(
                        max("water_level")
                )

                from(
                        "h2o_feet"
                )

                groupBy("location")
            })
        }

        assertThat(statement.toString()).isEqualTo("SELECT SUM(\"max\") FROM (SELECT MAX(\"water_level\") FROM \"h2o_feet\" GROUP BY \"location\")")
    }

    @Test
    fun example2() {
        val statement = select {
            fields(
                    mean("difference")
            )

            from(select {
                fields(
                        field("cats") - field("dogs") alias "difference"
                )

                from(
                        "pet_daycare"
                )
            })
        }

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"difference\") FROM (SELECT \"cats\" - \"dogs\" AS \"difference\" FROM \"pet_daycare\")")
    }

    @Test
    fun example3() {
        val statement = select {
            fields(
                    "all_the_means"
            )

            from(select {
                fields(
                        mean("water_level") alias "all_the_means"
                )

                from(
                        "h2o_feet"
                )

                where(
                        time() isAfter "2015-08-18T00:00:00Z"
                                and (time() isBefore "2015-08-18T00:30:00Z")
                )

                groupBy(time("12m"))
            })

            where(
                    field("all_the_means") isGreaterThan 5
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT \"all_the_means\" FROM (SELECT MEAN(\"water_level\") AS \"all_the_means\" FROM \"h2o_feet\" WHERE time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:30:00Z' GROUP BY time(12m)) WHERE \"all_the_means\" > 5")
    }

    @Test
    fun example4() {
        val statement = select {
            fields(
                    sum("water_level_derivative") alias "sum_derivative"
            )

            from(
                    select {
                        fields(
                                derivative(mean("water_level")) alias "water_level_derivative"
                        )

                        from("h2o_feet")

                        where(
                                time() isAfter "2015-08-18T00:00:00Z"
                                        and (time() isBefore "2015-08-18T00:30:00Z")
                        )

                        groupBy(
                                time("12m"), tag("location")
                        )
                    }
            )

            groupBy(
                    "location"
            )
        }

        assertThat(statement.toString()).isEqualTo("SELECT SUM(\"water_level_derivative\") AS \"sum_derivative\" FROM (SELECT DERIVATIVE(MEAN(\"water_level\")) AS \"water_level_derivative\" FROM \"h2o_feet\" WHERE time >= '2015-08-18T00:00:00Z' AND time <= '2015-08-18T00:30:00Z' GROUP BY time(12m), \"location\") GROUP BY \"location\"")
    }
}
