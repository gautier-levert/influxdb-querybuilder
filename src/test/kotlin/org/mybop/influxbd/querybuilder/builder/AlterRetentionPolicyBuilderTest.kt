package org.mybop.influxbd.querybuilder.builder

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

class AlterRetentionPolicyBuilderTest {
    @Test
    fun example() {
        // Set default retention policy for mydb to 1h.cpu.
        val statement = alterRetentionPolicy {
            named("1h.cpu")
            on("mydb")
            default()
        }

        assertThat(statement.toString()).isEqualTo("ALTER RETENTION POLICY \"1h.cpu\" ON \"mydb\" DEFAULT")
    }

    @Test
    fun example2() {
        // Change duration and replication factor.
        val statement = alterRetentionPolicy {
            named("policy1")
            on("somedb")
            duration("1h")
            replication(4)
        }

        assertThat(statement.toString()).isEqualTo("ALTER RETENTION POLICY \"policy1\" ON \"somedb\" DURATION 1h REPLICATION 4")
    }
}
