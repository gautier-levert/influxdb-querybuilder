package org.mybop.influxbd.querybuilder.builder;

import org.junit.Test;
import org.mybop.influxbd.querybuilder.domain.statement.Select;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mybop.influxbd.querybuilder.builder.FunctionExtensionsKt.count;
import static org.mybop.influxbd.querybuilder.builder.FunctionExtensionsKt.mean;
import static org.mybop.influxbd.querybuilder.builder.SelectExtensionsKt.select;
import static org.mybop.influxbd.querybuilder.builder.TimeExtensionsKt.time;
import static org.mybop.influxbd.querybuilder.builder.ValueExtentionsKt.tag;

public class JavaAdvancedGroupByBuilderTest {

    @Test
    public void example1() {
        final Select statement = select()
                .fields(
                        mean("water_level")
                )
                .from("h2o_feet")
                .where(
                        tag("location").isEqualTo("coyote_creek").and(time().isAfter("2015-08-18T00:06:00Z")).and(time().isBefore("2015-08-18T00:54:00Z"))
                )
                .groupBy(
                        time("18m").offset("6m")
                )
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"water_level\") FROM \"h2o_feet\" WHERE \"location\" = 'coyote_creek' AND time >= '2015-08-18T00:06:00Z' AND time <= '2015-08-18T00:54:00Z' GROUP BY time(18m, 6m)");
    }

    @Test
    public void example2() {
        final Select statement = select()
                .fields(
                        mean("water_level")
                )
                .from("h2o_feet")
                .where(
                        tag("location").isEqualTo("coyote_creek").and(time().isAfter("2015-08-18T00:06:00Z")).and(time().isBefore("2015-08-18T00:54:00Z"))
                )
                .groupBy(
                        time("18m").offset("-12m")
                )
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT MEAN(\"water_level\") FROM \"h2o_feet\" WHERE \"location\" = 'coyote_creek' AND time >= '2015-08-18T00:06:00Z' AND time <= '2015-08-18T00:54:00Z' GROUP BY time(18m, -12m)");
    }

    @Test
    public void example3() {
        final Select statement = select()
                .fields(
                        count("water_level")
                )
                .from("h2o_feet")
                .where(
                        tag("location").isEqualTo("coyote_creek").and(time().isAfter("2015-08-18T00:06:00Z")).and(time().isStrictlyBefore("2015-08-18T00:18:00Z"))
                )
                .groupBy(
                        time("12m").offset("6m")
                )
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT COUNT(\"water_level\") FROM \"h2o_feet\" WHERE \"location\" = 'coyote_creek' AND time >= '2015-08-18T00:06:00Z' AND time < '2015-08-18T00:18:00Z' GROUP BY time(12m, 6m)");
    }
}
