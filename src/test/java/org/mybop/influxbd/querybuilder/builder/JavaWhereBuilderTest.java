package org.mybop.influxbd.querybuilder.builder;

import org.junit.Test;
import org.mybop.influxbd.querybuilder.domain.statement.Select;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mybop.influxbd.querybuilder.builder.SelectExtensionsKt.select;
import static org.mybop.influxbd.querybuilder.builder.TimeExtensionsKt.now;
import static org.mybop.influxbd.querybuilder.builder.TimeExtensionsKt.time;
import static org.mybop.influxbd.querybuilder.builder.ValueExtentionsKt.field;
import static org.mybop.influxbd.querybuilder.builder.ValueExtentionsKt.tag;

public class JavaWhereBuilderTest {

    @Test
    public void specificFieldKeyValues() {
        //  Select data that have specific field key-values
        final Select statement = select()
                .from("h2o_feet")
                .where(
                        field("water_level").isGreaterThan(8)
                )
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\" WHERE \"water_level\" > 8");
    }

    @Test
    public void specificFieldKeyValues2() {
        // Select data that have a specific string field key-value
        final Select statement = select()
                .from("h2o_feet")
                .where(
                        field("level description").isEqualTo("below 3 feet")
                )
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\" WHERE \"level description\" = 'below 3 feet'");
    }

    @Test
    public void specificFieldKeyValuesBasicArithmetic() {
        // Select data that have a specific field key-value and perform basic arithmetic
        final Select statement = select()
                .from("h2o_feet")
                .where(
                        field("water_level").plus(2).isGreaterThan(11.9)
                )
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\" WHERE \"water_level\" + 2 > 11.9");
    }

    @Test
    public void specificFieldKeyAndTagValues() {
        // Select data that have specific field key-values and tag key-values
        final Select statement = select()
                .fields("water_level")
                .from("h2o_feet")
                .where(
                        tag("location").isNotEqualTo("santa_monica")
                                .and((field("water_level").isLessThan(-0.59).or(field("water_level").isGreaterThan(9.95))))
                )
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT \"water_level\" FROM \"h2o_feet\" WHERE \"location\" != 'santa_monica' AND (\"water_level\" < -0.59 OR \"water_level\" > 9.95)");
    }

    @Test
    public void specificTimestamp() {
        // Select data that have specific field key-values and tag key-values
        final Select statement = select()
                .from("h2o_feet")
                .where(
                        time().isStrictlyAfter(now().minus("7d"))
                )
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT * FROM \"h2o_feet\" WHERE time > now() - 7d");
    }
}
