package org.mybop.influxbd.querybuilder.builder;

import org.junit.Test;
import org.mybop.influxbd.querybuilder.domain.statement.Select;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mybop.influxbd.querybuilder.builder.FunctionExtensionsKt.max;
import static org.mybop.influxbd.querybuilder.builder.SelectExtensionsKt.select;
import static org.mybop.influxbd.querybuilder.builder.TimeExtensionsKt.time;
import static org.mybop.influxbd.querybuilder.builder.ValueExtentionsKt.tag;

public class JavaGroupByTimeAndFillBuilderTest {
    @Test
    public void example1() {
        final Select statement = select()
                .fields(
                        max("water_level")
                )

                .from("h2o_feet")

                .where(
                        tag("location").isEqualTo("coyote_creek").and(time().isAfter("2015-09-18T16:00:00Z")).and(time().isBefore("2015-09-18T16:42:00Z"))
                )

                .groupBy(
                        time("12m")
                )

                .fill(100)
                .build();

        assertThat(statement.toString()).isEqualTo("SELECT MAX(\"water_level\") FROM \"h2o_feet\" WHERE \"location\" = 'coyote_creek' AND time >= '2015-09-18T16:00:00Z' AND time <= '2015-09-18T16:42:00Z' GROUP BY time(12m) fill(100)");
    }
}
