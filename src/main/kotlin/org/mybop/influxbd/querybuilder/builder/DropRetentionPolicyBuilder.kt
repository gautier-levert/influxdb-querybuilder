package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.statement.DropRetentionPolicy

class DropRetentionPolicyBuilder {

    private var name: String? = null

    private var database: String? = null

    infix fun named(name: String): DropRetentionPolicyBuilder {
        this.name = name;
        return this
    }

    infix fun on(database: String): DropRetentionPolicyBuilder {
        this.database = database
        return this
    }

    fun build() = DropRetentionPolicy(
            policy = Name(name!!),
            on = On(Name(database!!))
    )

    fun buildString() = build().toString()
}
