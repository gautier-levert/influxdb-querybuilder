package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Field
import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.Tag
import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.FromMeasurement
import org.mybop.influxbd.querybuilder.domain.clause.FromQuery
import org.mybop.influxbd.querybuilder.domain.clause.GroupBy
import org.mybop.influxbd.querybuilder.domain.clause.Limit
import org.mybop.influxbd.querybuilder.domain.clause.OrderBy
import org.mybop.influxbd.querybuilder.domain.clause.Selectable
import org.mybop.influxbd.querybuilder.domain.clause.SerieLimit
import org.mybop.influxbd.querybuilder.domain.clause.Where
import org.mybop.influxbd.querybuilder.domain.condition.Condition
import org.mybop.influxbd.querybuilder.domain.group.Dimension
import org.mybop.influxbd.querybuilder.domain.group.FillOption
import org.mybop.influxbd.querybuilder.domain.selection.Fields
import org.mybop.influxbd.querybuilder.domain.selection.Selection
import org.mybop.influxbd.querybuilder.domain.selection.Star
import org.mybop.influxbd.querybuilder.domain.sort.Sort
import org.mybop.influxbd.querybuilder.domain.statement.Select
import org.mybop.influxbd.querybuilder.domain.time.StringTimezone
import org.mybop.influxbd.querybuilder.domain.time.Timezone
import org.mybop.influxbd.querybuilder.domain.value.DoubleValue
import org.mybop.influxbd.querybuilder.domain.value.FloatValue
import org.mybop.influxbd.querybuilder.domain.value.IntValue
import org.mybop.influxbd.querybuilder.domain.value.LongValue
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class SelectBuilder {

    private var selection: Selection = Star

    private var from: From? = null

    private var into: Measurement? = null

    private var where: Where? = null

    private var groupBy: GroupBy? = null

    private var orderBy: OrderBy? = null

    private var limit: Limit? = null

    private var serielimit: SerieLimit? = null

    private var timezone: Timezone? = null

    fun allFields(): SelectBuilder {
        selection = Star
        return this
    }

    fun fields(field: Selectable, vararg others: Selectable): SelectBuilder {
        selection = Fields(field, *others)
        return this
    }

    fun fields(field: String, vararg others: String) = fields(Field(field), *others.map { Field(it) }.toTypedArray())

    fun from(from: From): SelectBuilder {
        this.from = from
        return this
    }

    fun from(measurement: Measurement, vararg others: Measurement) = from(FromMeasurement(measurement, *others))

    fun from(measurement: String, vararg others: String) = from(measurement(measurement), *others.map { measurement(it) }.toTypedArray())

    fun from(subquery: Select) = from(FromQuery(subquery))

    fun into(measurement: Measurement): SelectBuilder {
        into = measurement
        return this
    }

    fun into(measurement: String) = into(measurement(measurement))

    fun where(condition: Condition): SelectBuilder {
        where = Where(condition)
        return this
    }

    fun groupBy(dimension: Dimension, vararg others: Dimension): SelectBuilder {
        groupBy = GroupBy(dimension, *others)
        return this
    }

    fun groupByAllTags() = groupBy(Star)

    fun groupBy(key: String, vararg others: String) = groupBy(Tag(key), *others.map { Tag(it) }.toTypedArray())

    fun fill(option: FillOption): SelectBuilder {
        groupBy!!.fill = option
        return this
    }

    fun fill(option: Int) = fill(IntValue(option))

    fun fill(option: Long) = fill(LongValue(option))

    fun fill(option: Float) = fill(FloatValue(option))

    fun fill(option: Double) = fill(DoubleValue(option))

    fun orderBy(sort: Sort, vararg others: Sort): SelectBuilder {
        orderBy = OrderBy(sort, *others)
        return this
    }

    fun limit(number: Long): SelectBuilder {
        limit = Limit(number)
        return this
    }

    fun limit(number: Int) = limit(number.toLong())

    fun offset(number: Long): SelectBuilder {
        limit!!.offset = number
        return this
    }

    fun offset(number: Int) = offset(number.toLong())

    fun serieLimit(number: Long): SelectBuilder {
        serielimit = SerieLimit(number)
        return this
    }

    fun serieLimit(number: Int) = serieLimit(number.toLong())

    fun serieOffset(number: Long): SelectBuilder {
        serielimit!!.offset = number
        return this
    }

    fun serieOffset(number: Int) = serieOffset(number.toLong())

    fun timezone(timezone: Timezone): SelectBuilder {
        this.timezone = timezone
        return this
    }

    fun timezone(timezone: String) = timezone(StringTimezone(StringValue(timezone)))

    fun build() = Select(
            fields = selection,
            from = from!!,
            into = into,
            where = where,
            groupBy = groupBy,
            orderBy = orderBy,
            limit = limit,
            serielimit = serielimit,
            timezone = timezone
    )

    fun buildString() = build().toString()
}
