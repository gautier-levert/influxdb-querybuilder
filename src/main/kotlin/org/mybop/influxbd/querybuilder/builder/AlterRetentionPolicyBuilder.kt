package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.statement.AlterRetentionPolicy
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration
import org.mybop.influxbd.querybuilder.domain.time.duration.StringDuration

class AlterRetentionPolicyBuilder {

    private var name: String? = null

    private var database: String? = null

    private var duration: Duration? = null

    private var replication: Int? = null

    private var shardDuration: Duration? = null

    private var default: Boolean = false

    infix fun named(name: String): AlterRetentionPolicyBuilder {
        this.name = name;
        return this
    }

    infix fun on(database: String): AlterRetentionPolicyBuilder {
        this.database = database
        return this
    }

    infix fun duration(duration: Duration): AlterRetentionPolicyBuilder {
        this.duration = duration
        return this
    }

    infix fun duration(duration: String) = duration(StringDuration(duration))

    infix fun replication(replication: Int): AlterRetentionPolicyBuilder {
        this.replication = replication
        return this
    }

    infix fun shardDuration(shardDuration: Duration): AlterRetentionPolicyBuilder {
        this.shardDuration = shardDuration
        return this
    }

    fun default(default: Boolean = true): AlterRetentionPolicyBuilder {
        this.default = default
        return this
    }

    fun build() = AlterRetentionPolicy(
            policyName = Name(name!!),
            on = On(Name(database!!)),
            duration = duration,
            replication = replication,
            shardDuration = shardDuration,
            default = default
    )

    fun buildString() = build().toString()
}
