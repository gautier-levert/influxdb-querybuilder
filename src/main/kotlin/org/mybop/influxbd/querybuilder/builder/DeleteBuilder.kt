package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.FromMeasurement
import org.mybop.influxbd.querybuilder.domain.clause.Where
import org.mybop.influxbd.querybuilder.domain.condition.Condition
import org.mybop.influxbd.querybuilder.domain.statement.Delete

class DeleteBuilder {

    private var from: From? = null

    private var where: Where? = null

    fun from(from: From): DeleteBuilder {
        this.from = from
        return this
    }

    fun from(measurement: Measurement, vararg others: Measurement) = from(FromMeasurement(measurement, *others))

    fun from(measurement: String, vararg others: String) = from(measurement(measurement), *others.map { measurement(it) }.toTypedArray())

    fun where(condition: Condition): DeleteBuilder {
        where = Where(condition)
        return this
    }

    fun build() = Delete(
            from = from!!,
            where = where
    )

    fun buildString() = build().toString()
}
