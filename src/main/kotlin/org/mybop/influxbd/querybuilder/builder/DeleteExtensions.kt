package org.mybop.influxbd.querybuilder.builder

fun delete() = DeleteBuilder()

fun delete(init: DeleteBuilder.() -> Unit = {}) =
        DeleteBuilder().apply(init).build()
