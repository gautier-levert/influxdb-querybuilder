package org.mybop.influxbd.querybuilder.builder

fun alterRetentionPolicy() = AlterRetentionPolicyBuilder()
fun alterRetentionPolicy(init: AlterRetentionPolicyBuilder.() -> Unit = {}) = AlterRetentionPolicyBuilder().apply(init).build()

fun createRetentionPolicy() = CreateRetentionPolicyBuilder()
fun createRetentionPolicy(init: CreateRetentionPolicyBuilder.() -> Unit = {}) = CreateRetentionPolicyBuilder().apply(init).build()

fun dropRetentionPolicy() = DropRetentionPolicyBuilder()
fun dropRetentionPolicy(init: DropRetentionPolicyBuilder.() -> Unit = {}) = DropRetentionPolicyBuilder().apply(init).build()
