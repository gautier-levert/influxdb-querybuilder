package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Field
import org.mybop.influxbd.querybuilder.domain.Tag
import org.mybop.influxbd.querybuilder.domain.selection.Star
import org.mybop.influxbd.querybuilder.domain.value.DoubleValue
import org.mybop.influxbd.querybuilder.domain.value.FloatValue
import org.mybop.influxbd.querybuilder.domain.value.IntValue
import org.mybop.influxbd.querybuilder.domain.value.LongValue
import org.mybop.influxbd.querybuilder.domain.value.StringValue

fun all() = Star

fun tag(name: String) = Tag(name)

fun field(name: String) = Field(name)

fun value(value: String) = StringValue(value)

fun value(value: Int) = IntValue(value)

fun value(value: Long) = LongValue(value)

fun value(value: Float) = FloatValue(value)

fun value(value: Double) = DoubleValue(value)
