package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Field
import org.mybop.influxbd.querybuilder.domain.Tag
import org.mybop.influxbd.querybuilder.domain.function.Derivable
import org.mybop.influxbd.querybuilder.domain.function.aggregation.Count
import org.mybop.influxbd.querybuilder.domain.function.aggregation.Distinct
import org.mybop.influxbd.querybuilder.domain.function.aggregation.Integral
import org.mybop.influxbd.querybuilder.domain.function.aggregation.Mean
import org.mybop.influxbd.querybuilder.domain.function.aggregation.Median
import org.mybop.influxbd.querybuilder.domain.function.aggregation.Mode
import org.mybop.influxbd.querybuilder.domain.function.aggregation.Spread
import org.mybop.influxbd.querybuilder.domain.function.aggregation.StdDev
import org.mybop.influxbd.querybuilder.domain.function.aggregation.Sum
import org.mybop.influxbd.querybuilder.domain.function.selector.Bottom
import org.mybop.influxbd.querybuilder.domain.function.selector.First
import org.mybop.influxbd.querybuilder.domain.function.selector.Last
import org.mybop.influxbd.querybuilder.domain.function.selector.Max
import org.mybop.influxbd.querybuilder.domain.function.selector.Min
import org.mybop.influxbd.querybuilder.domain.function.selector.Percentile
import org.mybop.influxbd.querybuilder.domain.function.selector.Sample
import org.mybop.influxbd.querybuilder.domain.function.selector.Top
import org.mybop.influxbd.querybuilder.domain.function.transformation.CumulativeSum
import org.mybop.influxbd.querybuilder.domain.function.transformation.Derivative
import org.mybop.influxbd.querybuilder.domain.function.transformation.Difference
import org.mybop.influxbd.querybuilder.domain.function.transformation.Elapsed
import org.mybop.influxbd.querybuilder.domain.function.transformation.MovingAverage
import org.mybop.influxbd.querybuilder.domain.function.transformation.NonNegativeDerivative
import org.mybop.influxbd.querybuilder.domain.function.transformation.NonNegativeDifference
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration

fun count(field: Field) = Count(field)
fun count(field: String) = count(Field(field))

fun distinct(field: Field) = Distinct(field)
fun distinct(field: String) = distinct(Field(field))

fun integral(field: Field, unit: Duration? = null) = Integral(field, unit)
fun integral(field: String, unit: Duration? = null) = integral(Field(field), unit)

fun mean(field: Field) = Mean(field)
fun mean(field: String) = mean(Field(field))

fun median(field: Field) = Median(field)
fun median(field: String) = median(Field(field))

fun mode(field: Field) = Mode(field)
fun mode(field: String) = mode(Field(field))

fun spread(field: Field) = Spread(field)
fun spread(field: String) = spread(Field(field))

fun stdDev(field: Field) = StdDev(field)
fun stdDev(field: String) = stdDev(Field(field))

fun sum(field: Field) = Sum(field)
fun sum(field: String) = sum(Field(field))

fun bottom(field: Field, number: Int, vararg tags: Tag) = Bottom(field, number, *tags)
fun bottom(field: String, number: Int, vararg tags: String) = bottom(Field(field), number, *tags.map { Tag(it) }.toTypedArray())

fun top(field: Field, number: Int, vararg tags: Tag) = Top(field, number, *tags)
fun top(field: String, number: Int, vararg tags: String) = top(Field(field), number, *tags.map { Tag(it) }.toTypedArray())

fun min(field: Field) = Min(field)
fun min(field: String) = min(Field(field))

fun max(field: Field) = Max(field)
fun max(field: String) = max(Field(field))

fun first(field: Field) = First(field)
fun first(field: String) = first(Field(field))

fun last(field: Field) = Last(field)
fun last(field: String) = last(Field(field))

fun percentile(field: Field, number: Int) = Percentile(field, number)
fun percentile(field: String, number: Int) = percentile(Field(field), number)

fun sample(field: Field, number: Int) = Sample(field, number)
fun sample(field: String, number: Int) = sample(Field(field), number)

fun cumulativeSum(field: Field) = CumulativeSum(field)
fun cumulativeSum(field: String) = cumulativeSum(Field(field))

fun derivative(field: Derivable, unit: Duration? = null) = Derivative(field, unit)
fun derivative(field: String, unit: Duration? = null) = derivative(Field(field), unit)

fun nonNegativeDerivative(field: Field, unit: Duration? = null) = NonNegativeDerivative(field, unit)
fun nonNegativeDerivative(field: String, unit: Duration? = null) = nonNegativeDerivative(Field(field), unit)

fun difference(field: Field) = Difference(field)
fun difference(field: String) = difference(Field(field))

fun nonNegativeDifference(field: Field) = NonNegativeDifference(field)
fun nonNegativeDifference(field: String) = nonNegativeDifference(Field(field))

fun elapsed(field: Field) = Elapsed(field)
fun elapsed(field: String) = elapsed(Field(field))

fun movingAverage(field: Field, number: Int) = MovingAverage(field, number)
fun movingAverage(field: String, number: Int) = movingAverage(Field(field), number)
