package org.mybop.influxbd.querybuilder.builder

import org.mybop.influxbd.querybuilder.domain.Key
import org.mybop.influxbd.querybuilder.domain.clause.Aliasable
import org.mybop.influxbd.querybuilder.domain.condition.TimeCondition
import org.mybop.influxbd.querybuilder.domain.condition.TimeOperator
import org.mybop.influxbd.querybuilder.domain.sort.Order
import org.mybop.influxbd.querybuilder.domain.sort.SortTime
import org.mybop.influxbd.querybuilder.domain.time.instant.Instant
import org.mybop.influxbd.querybuilder.domain.time.instant.StringInstant

class TimeBuilder : Key("time"), Aliasable {

    infix fun isBefore(instant: Instant) = TimeCondition(TimeOperator.BEFORE, instant)

    infix fun isBefore(instant: String) = isBefore(StringInstant(instant))

    infix fun isStrictlyBefore(instant: Instant) = TimeCondition(TimeOperator.STRICTLY_BEFORE, instant)

    infix fun isStrictlyBefore(instant: String) = isStrictlyBefore(StringInstant(instant))

    infix fun isAfter(instant: Instant) = TimeCondition(TimeOperator.AFTER, instant)

    infix fun isAfter(instant: String) = isAfter(StringInstant(instant))

    infix fun isStrictlyAfter(instant: Instant) = TimeCondition(TimeOperator.STRICTLY_AFTER, instant)

    infix fun isStrictlyAfter(instant: String) = isStrictlyAfter(StringInstant(instant))

    fun desc() = SortTime(Order.DESC)

    fun asc() = SortTime(Order.ASC)
}
