package org.mybop.influxbd.querybuilder.domain.condition

enum class ValueOperator(private val representation: String) {

    EQUAL_TO("="),
    NOT_EQUAL_TO("!="),
    GREATER_THAN(">"),
    GREATER_THAN_OR_EQUAL_TO(">="),
    LESS_THAN("<"),
    LESS_THAN_OR_EQUAL_TO("<=");

    override fun toString(): String = representation
}

