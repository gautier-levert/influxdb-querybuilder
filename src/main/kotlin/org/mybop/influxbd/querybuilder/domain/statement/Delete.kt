package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.Where

class Delete(
        val from: From? = null,
        val where: Where? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("DELETE")
        from?.also {
            sb.append(" $it")
        }
        where?.also {
            sb.append(" $it")
        }
        return sb.toString()
    }
}
