package org.mybop.influxbd.querybuilder.domain

import org.mybop.influxbd.querybuilder.domain.arithmetic.ArithmeticValue
import org.mybop.influxbd.querybuilder.domain.clause.Selectable
import org.mybop.influxbd.querybuilder.domain.function.Derivable
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg
import org.mybop.influxbd.querybuilder.domain.sort.Order
import org.mybop.influxbd.querybuilder.domain.sort.Sort
import org.mybop.influxbd.querybuilder.domain.sort.SortField

class Field(name: String) : Key(name), ArithmeticValue, Sort, FunctionArg, Derivable, Selectable {

    fun desc() = SortField(this, Order.DESC)

    fun asc() = SortField(this, Order.ASC)
}
