package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.clause.Limit
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.clause.Where

class ShowMeasurements(
        val on: On? = null,
        val measurement: Measurement? = null,
        val where: Where? = null,
        val limit: Limit? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("SHOW MEASUREMENTS")

        on?.also {
            sb.append(" $it")
        }

        measurement?.also {
            sb.append(" WITH MEASUREMENT = $measurement")
        }

        where?.also {
            sb.append(" $it")
        }

        limit?.also {
            sb.append(" $it")
        }

        return sb.toString()
    }
}
