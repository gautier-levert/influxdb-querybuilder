package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Measurement
import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.GroupBy
import org.mybop.influxbd.querybuilder.domain.clause.Limit
import org.mybop.influxbd.querybuilder.domain.clause.OrderBy
import org.mybop.influxbd.querybuilder.domain.clause.SerieLimit
import org.mybop.influxbd.querybuilder.domain.clause.Where
import org.mybop.influxbd.querybuilder.domain.selection.Selection
import org.mybop.influxbd.querybuilder.domain.time.Timezone

class Select(
        val fields: Selection,
        val from: From,
        val into: Measurement? = null,
        val where: Where? = null,
        val groupBy: GroupBy? = null,
        val orderBy: OrderBy? = null,
        val limit: Limit? = null,
        val serielimit: SerieLimit? = null,
        val timezone: Timezone? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("SELECT $fields")

        into?.also {
            sb.append(" INTO $into")
        }

        sb.append(" $from");

        where?.also {
            sb.append(" $where")
        }

        groupBy?.also {
            sb.append(" $groupBy")
        }

        orderBy?.also {
            sb.append(" $orderBy")
        }

        limit?.also {
            sb.append(" $limit")
        }

        serielimit?.also {
            sb.append(" $serielimit")
        }

        timezone?.also {
            sb.append(" $timezone")
        }

        return sb.toString()
    }
}
