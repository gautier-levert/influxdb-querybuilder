package org.mybop.influxbd.querybuilder.domain.condition

enum class JoinOperator(private val representation: String) {
    AND("AND"),
    OR("OR");

    override fun toString(): String = representation


}
