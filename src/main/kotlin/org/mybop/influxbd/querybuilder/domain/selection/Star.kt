package org.mybop.influxbd.querybuilder.domain.selection

import org.mybop.influxbd.querybuilder.domain.function.FunctionArg
import org.mybop.influxbd.querybuilder.domain.group.Dimension

object Star : Selection, Dimension, FunctionArg {
    override fun toString(): String = "*"
}
