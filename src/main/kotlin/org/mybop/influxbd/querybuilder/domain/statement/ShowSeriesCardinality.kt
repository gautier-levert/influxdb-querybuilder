package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.GroupBy
import org.mybop.influxbd.querybuilder.domain.clause.Limit
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.clause.Where

class ShowSeriesCardinality(
        val exact: Boolean = false,
        val on: On? = null,
        val from: From? = null,
        val where: Where? = null,
        val groupBy: GroupBy? = null,
        val limit: Limit? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("SHOW SERIES")

        if (exact) {
            sb.append(" EXACT")
        }

        sb.append(" CARDINALITY")

        on?.also {
            sb.append(" $it")
        }

        from?.also {
            sb.append(" $it")
        }

        where?.also {
            sb.append(" $it")
        }

        groupBy?.also {
            sb.append(" $it")
        }

        limit?.also {
            sb.append(" $it")
        }

        return sb.toString()
    }
}
