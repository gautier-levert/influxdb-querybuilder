package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration

class CreateContinuousQuery(
        val queryName: Name,
        val on: On,
        val select: Select,
        val every: Duration? = null,
        val `for`: Duration? = null
) {

    val resample: Boolean
        get() = every != null || `for` != null

    override fun toString(): String {
        val sb = StringBuilder("CREATE CONTINUOUS QUERY $queryName $on")

        if (resample) {
            sb.append(" RESAMPLE")
            every?.also {
                sb.append(" EVERY $it")
            }
            `for`?.also {
                sb.append(" FOR $it")
            }
        }

        sb.append(" BEGIN $select END")

        return sb.toString()
    }
}
