package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration

class CreateDatabase(
        val databaseName: Name,
        val retentionPolicyDuration: Duration? = null,
        val retentionPolicyReplication: Int? = null,
        val retentionPolicyShardGroupDuration: Duration? = null,
        val retentionPolicyName: Name? = null
) {

    val withRetentionPolicy: Boolean
        get() = retentionPolicyDuration != null
                || retentionPolicyReplication != null
                || retentionPolicyShardGroupDuration != null
                || retentionPolicyName != null

    override fun toString(): String {
        val sb = StringBuilder("CREATE DATABASE $databaseName")

        if (withRetentionPolicy) {
            sb.append(" WITH")
            retentionPolicyDuration?.also {
                sb.append(" DURATION $it")
            }
            retentionPolicyReplication?.also {
                sb.append(" REPLICATION $it")
            }
            retentionPolicyShardGroupDuration?.also {
                sb.append(" SHARD DURATION $it")
            }
            retentionPolicyName?.also {
                sb.append(" NAME $it")
            }
        }

        return sb.toString()
    }
}
