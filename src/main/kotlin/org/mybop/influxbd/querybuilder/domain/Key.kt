package org.mybop.influxbd.querybuilder.domain

/**
 * Identifiers are tokens which refer to database names, retention policy names, user names, measurement names, tag keys, and field keys.
 */
abstract class Key(val name: String) {

    val escapedName
        get() = name.replace("\"", "\\\"")

    override fun toString(): String = "\"$escapedName\""
}
