package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.statement.Select

class FromQuery(
        val query: Select
) : From {
    override fun toString() = "FROM ($query)"
}
