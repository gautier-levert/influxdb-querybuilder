package org.mybop.influxbd.querybuilder.domain.selection

import org.mybop.influxbd.querybuilder.domain.Name

class Alias(val key: Name) {

    override fun toString() = "AS $key"
}
