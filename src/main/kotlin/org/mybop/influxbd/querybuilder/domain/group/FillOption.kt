package org.mybop.influxbd.querybuilder.domain.group

interface FillOption {
    override fun toString(): String
}

object None : FillOption {
    override fun toString() = "none"
}

object Null : FillOption {
    override fun toString() = "null"
}

object Previous : FillOption {
    override fun toString() = "previous"
}

object Linear : FillOption {
    override fun toString() = "linear"
}
