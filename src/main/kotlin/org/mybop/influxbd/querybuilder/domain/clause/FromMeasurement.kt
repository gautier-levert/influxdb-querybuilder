package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.Measurement

class FromMeasurement(
        measurement: Measurement,
        vararg others: Measurement
) : From {

    val measurements = listOf(measurement, *others)

    override fun toString(): String {
        return "FROM ${measurements.joinToString(", ")}"
    }
}
