package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.Tag

class WihKeyNotEqualTo(
        val tagKey: Tag
) : WithKey {
    override fun toString() = "WITH KEY != $tagKey"
}
