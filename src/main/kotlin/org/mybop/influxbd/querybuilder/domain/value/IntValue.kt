package org.mybop.influxbd.querybuilder.domain.value

import org.mybop.influxbd.querybuilder.domain.arithmetic.ArithmeticValue
import org.mybop.influxbd.querybuilder.domain.group.FillOption

class IntValue(val value: Int) : ArithmeticValue, FillOption {
    override fun toString(): String = value.toString()
}
