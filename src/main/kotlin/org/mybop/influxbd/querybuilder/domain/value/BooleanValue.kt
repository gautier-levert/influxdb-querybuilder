package org.mybop.influxbd.querybuilder.domain.value

class BooleanValue(
        val value: Boolean
) : Value {
    override fun toString() = value.toString()
}
