package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Measurement

class DropMeasurement(
        val measurement: Measurement
) {

    override fun toString(): String {
        return "DROP MEASUREMENT $measurement"
    }
}
