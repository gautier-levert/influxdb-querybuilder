package org.mybop.influxbd.querybuilder.domain.value

import org.mybop.influxbd.querybuilder.domain.arithmetic.ArithmeticValue
import org.mybop.influxbd.querybuilder.domain.group.FillOption

class DoubleValue(val value: Double) : ArithmeticValue, FillOption {
    override fun toString(): String = value.toString()
}
