package org.mybop.influxbd.querybuilder.domain.statement

class Explain(
        val select: Select,
        val analyze: Boolean = false
) {

    override fun toString(): String {
        val sb = StringBuilder("EXPLAIN")

        if (analyze) {
            sb.append(" ANALYZE")
        }

        sb.append(" $select")

        return sb.toString()
    }
}
