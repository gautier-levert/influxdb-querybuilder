package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.condition.Condition

class Where(
        val condition: Condition
) {

    override fun toString(): String {
        return "WHERE $condition"
    }
}
