package org.mybop.influxbd.querybuilder.domain.time

import org.mybop.influxbd.querybuilder.domain.value.StringValue

class StringTimezone(val timezone: StringValue) : Timezone {

    override fun toString() = "tz($timezone)"
}
