package org.mybop.influxbd.querybuilder.domain.clause

class SerieLimit(
        val number: Long,
        var offset: Long? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("SLIMIT $number")

        offset?.also {
            sb.append(" SOFFSET $it")
        }

        return sb.toString()
    }
}
