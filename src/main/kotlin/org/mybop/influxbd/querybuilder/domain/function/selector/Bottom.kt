package org.mybop.influxbd.querybuilder.domain.function.selector

import org.mybop.influxbd.querybuilder.domain.Tag
import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class Bottom(
        private val field: FunctionArg,
        private val n: Int,
        private vararg val keys: Tag
) : Function {

    override fun toString() = "BOTTOM(${listOf(field, *keys, n).joinToString(separator = ", ")})"
}
