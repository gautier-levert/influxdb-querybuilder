package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name

class DropDatabase(
        val database: Name
) {

    override fun toString(): String {
        return "DROP DATABASE $database"
    }
}
