package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.Name

class On(val database: Name) {
    override fun toString() = "ON $database"
}
