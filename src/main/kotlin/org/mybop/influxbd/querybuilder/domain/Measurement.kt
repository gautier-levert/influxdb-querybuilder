package org.mybop.influxbd.querybuilder.domain

class Measurement(
        val measurementName: Name,
        val retentionPolicyName: Name? = null,
        val databaseName: Name? = null
) {

    override fun toString(): String =
            if (databaseName != null && retentionPolicyName != null) {
                "$databaseName.$retentionPolicyName.$measurementName"
            } else if (databaseName != null && retentionPolicyName == null) {
                "$databaseName..$measurementName"
            } else if (databaseName == null && retentionPolicyName != null) {
                "$retentionPolicyName.$measurementName"
            } else {
                "$measurementName"
            }
}
