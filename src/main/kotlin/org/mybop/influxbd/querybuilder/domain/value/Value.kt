package org.mybop.influxbd.querybuilder.domain.value

import org.mybop.influxbd.querybuilder.domain.clause.Aliasable
import org.mybop.influxbd.querybuilder.domain.clause.Selectable
import org.mybop.influxbd.querybuilder.domain.condition.ValueCondition
import org.mybop.influxbd.querybuilder.domain.condition.ValueOperator

interface Value : Aliasable, Selectable {

    private fun condition(operator: ValueOperator, right: Value) = ValueCondition(left = this, operator = operator, right = right)

    infix fun isEqualTo(other: Value) = condition(ValueOperator.EQUAL_TO, other)

    infix fun isEqualTo(other: Boolean) = isEqualTo(BooleanValue(other))

    infix fun isEqualTo(other: String) = isEqualTo(StringValue(other))

    infix fun isEqualTo(other: Int) = isEqualTo(IntValue(other))

    infix fun isEqualTo(other: Long) = isEqualTo(LongValue(other))

    infix fun isEqualTo(other: Float) = isEqualTo(FloatValue(other))

    infix fun isEqualTo(other: Double) = isEqualTo(DoubleValue(other))

    infix fun isNotEqualTo(other: Value) = condition(ValueOperator.NOT_EQUAL_TO, other)

    infix fun isNotEqualTo(other: Boolean) = isNotEqualTo(BooleanValue(other))

    infix fun isNotEqualTo(other: String) = isNotEqualTo(StringValue(other))

    infix fun isNotEqualTo(other: Int) = isNotEqualTo(IntValue(other))

    infix fun isNotEqualTo(other: Long) = isNotEqualTo(LongValue(other))

    infix fun isNotEqualTo(other: Float) = isNotEqualTo(FloatValue(other))

    infix fun isNotEqualTo(other: Double) = isNotEqualTo(DoubleValue(other))

    infix fun isGreaterThan(other: Value) = condition(ValueOperator.GREATER_THAN, other)

    infix fun isGreaterThan(other: String) = isGreaterThan(StringValue(other))

    infix fun isGreaterThan(other: Int) = isGreaterThan(IntValue(other))

    infix fun isGreaterThan(other: Long) = isGreaterThan(LongValue(other))

    infix fun isGreaterThan(other: Float) = isGreaterThan(FloatValue(other))

    infix fun isGreaterThan(other: Double) = isGreaterThan(DoubleValue(other))

    infix fun isGreaterThanOrEqualTo(other: Value) = condition(ValueOperator.GREATER_THAN_OR_EQUAL_TO, other)

    infix fun isGreaterThanOrEqualTo(other: String) = isGreaterThanOrEqualTo(StringValue(other))

    infix fun isGreaterThanOrEqualTo(other: Int) = isGreaterThanOrEqualTo(IntValue(other))

    infix fun isGreaterThanOrEqualTo(other: Long) = isGreaterThanOrEqualTo(LongValue(other))

    infix fun isGreaterThanOrEqualTo(other: Float) = isGreaterThanOrEqualTo(FloatValue(other))

    infix fun isGreaterThanOrEqualTo(other: Double) = isGreaterThanOrEqualTo(DoubleValue(other))

    infix fun isLessThan(other: Value) = condition(ValueOperator.LESS_THAN, other)

    infix fun isLessThan(other: String) = isLessThan(StringValue(other))

    infix fun isLessThan(other: Int) = isLessThan(IntValue(other))

    infix fun isLessThan(other: Long) = isLessThan(LongValue(other))

    infix fun isLessThan(other: Float) = isLessThan(FloatValue(other))

    infix fun isLessThan(other: Double) = isLessThan(DoubleValue(other))

    infix fun isLessThanOrEqualTo(other: Value) = condition(ValueOperator.LESS_THAN_OR_EQUAL_TO, other)

    infix fun isLessThanOrEqualTo(other: String) = isLessThanOrEqualTo(StringValue(other))

    infix fun isLessThanOrEqualTo(other: Int) = isLessThanOrEqualTo(IntValue(other))

    infix fun isLessThanOrEqualTo(other: Long) = isLessThanOrEqualTo(LongValue(other))

    infix fun isLessThanOrEqualTo(other: Float) = isLessThanOrEqualTo(FloatValue(other))

    infix fun isLessThanOrEqualTo(other: Double) = isLessThanOrEqualTo(DoubleValue(other))

    fun isTrue() = isEqualTo(true)

    fun isFalse() = isEqualTo(false)

    override fun toString(): String
}
