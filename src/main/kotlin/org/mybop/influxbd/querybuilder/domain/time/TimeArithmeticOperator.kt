package org.mybop.influxbd.querybuilder.domain.time;

enum class TimeArithmeticOperator(private val representation: String) {
    ADDITION("+"),
    SUBTRACTION("-");

    override fun toString() = representation
}

