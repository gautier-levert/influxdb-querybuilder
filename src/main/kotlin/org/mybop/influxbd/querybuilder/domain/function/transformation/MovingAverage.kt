package org.mybop.influxbd.querybuilder.domain.function.transformation

import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class MovingAverage(
        val field: FunctionArg,
        val number: Int
) : Function {
    override fun toString() = "MOVING_AVERAGE($field, $number)"
}
