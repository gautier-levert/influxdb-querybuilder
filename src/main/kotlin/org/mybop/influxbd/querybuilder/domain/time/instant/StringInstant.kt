package org.mybop.influxbd.querybuilder.domain.time.instant

class StringInstant(private val instant: String) : Instant {

    override fun toString() = "'$instant'"
}
