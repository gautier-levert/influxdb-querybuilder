package org.mybop.influxbd.querybuilder.domain.function.selector

import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class Sample(
        private val key: FunctionArg,
        private val n: Int
) {

    override fun toString() = "SAMPLE($key, $n)"
}
