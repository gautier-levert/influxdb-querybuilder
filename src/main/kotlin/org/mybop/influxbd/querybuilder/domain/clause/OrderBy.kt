package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.sort.Sort

class OrderBy(
        sortField: Sort,
        vararg others: Sort
) {

    val fields = listOf(sortField, *others)

    override fun toString(): String {
        return "ORDER BY ${fields.joinToString(", ")}"
    }
}
