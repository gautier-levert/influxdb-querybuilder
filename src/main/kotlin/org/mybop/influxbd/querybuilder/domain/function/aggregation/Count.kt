package org.mybop.influxbd.querybuilder.domain.function.aggregation

import org.mybop.influxbd.querybuilder.domain.function.Derivable
import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

/**
 * Returns the number of field values associated with the field key.
 */
class Count(val selection: FunctionArg) : Function, Derivable {

    override fun toString() = "COUNT($selection)"
}
