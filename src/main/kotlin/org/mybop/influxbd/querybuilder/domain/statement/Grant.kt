package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.Privilege
import org.mybop.influxbd.querybuilder.domain.clause.On

class Grant(
        val privilege: Privilege,
        val user: Name,
        val on: On? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("GRANT $privilege")

        on?.also {
            sb.append(" $it")
        }

        sb.append(" TO $user")

        return sb.toString()
    }
}


