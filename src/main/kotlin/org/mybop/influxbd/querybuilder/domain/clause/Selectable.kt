package org.mybop.influxbd.querybuilder.domain.clause

interface Selectable {
    override fun toString(): String
}
