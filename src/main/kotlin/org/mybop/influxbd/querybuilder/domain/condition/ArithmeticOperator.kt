package org.mybop.influxbd.querybuilder.domain.arithmetic

enum class ArithmeticOperator(private val representation: String) {
    ADDITION("+"),
    SUBTRACTION("-"),
    MULTIPLICATION("*"),
    DIVISION("/"),
    MODULO("%"),
    BITWISE_AND("&"),
    BITWISE_OR("|"),
    BITWISE_XOR("^");

    override fun toString() = representation
}
