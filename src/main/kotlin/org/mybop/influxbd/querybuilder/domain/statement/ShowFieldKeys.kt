package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.On

class ShowFieldKeys(
        val on: On? = null,
        val from: From? = null
) {
    override fun toString(): String {
        val sb = StringBuilder("SHOW FIELD KEYS")

        on?.also {
            sb.append(" $it")
        }

        from?.also {
            sb.append(" $it")
        }

        return sb.toString()
    }
}
