package org.mybop.influxbd.querybuilder.domain.statement

class ShowSubscriptions {
    override fun toString() = "SHOW SUBSCRIPTIONS"
}
