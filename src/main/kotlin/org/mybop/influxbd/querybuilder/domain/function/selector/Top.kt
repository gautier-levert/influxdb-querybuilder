package org.mybop.influxbd.querybuilder.domain.function.selector

import org.mybop.influxbd.querybuilder.domain.Key
import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class Top(
        private val field: FunctionArg,
        private val n: Int,
        private vararg val keys: Key
) : Function {

    override fun toString() = "TOP(${listOf(field, *keys, n).joinToString(separator = ", ")})"
}
