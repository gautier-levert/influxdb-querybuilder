package org.mybop.influxbd.querybuilder.domain.clause

class Limit(
        val number: Long,
        var offset: Long? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("LIMIT $number")

        offset?.also {
            sb.append(" OFFSET $it")
        }

        return sb.toString()
    }
}
