package org.mybop.influxbd.querybuilder.domain.function.aggregation

import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class Spread(private val selection: FunctionArg) : Function {

    override fun toString() = "SPREAD($selection)"
}
