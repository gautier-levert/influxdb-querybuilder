package org.mybop.influxbd.querybuilder.domain.sort

class SortTime(
        val order: Order? = null
) : Sort {
    override fun toString(): String {
        val sb = StringBuilder("time")

        order?.also {
            sb.append(" $it")
        }

        return sb.toString()
    }
}
