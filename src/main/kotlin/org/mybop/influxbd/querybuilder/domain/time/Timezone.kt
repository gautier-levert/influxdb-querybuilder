package org.mybop.influxbd.querybuilder.domain.time

interface Timezone {

    override fun toString(): String
}
