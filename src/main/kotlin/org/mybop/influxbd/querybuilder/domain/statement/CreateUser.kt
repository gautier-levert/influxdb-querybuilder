package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class CreateUser(
        val user: Name,
        val password: StringValue,
        val allPrivileges: Boolean = false
) {

    override fun toString(): String {
        val sb = StringBuilder("CREATE USER $user WITH PASSWORD $password")
        if (allPrivileges) {
            sb.append(" WITH ALL PRIVILEGES")
        }
        return sb.toString()
    }
}
