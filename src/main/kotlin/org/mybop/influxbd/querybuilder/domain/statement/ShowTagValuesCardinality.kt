package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.clause.From
import org.mybop.influxbd.querybuilder.domain.clause.GroupBy
import org.mybop.influxbd.querybuilder.domain.clause.Limit
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.clause.Where
import org.mybop.influxbd.querybuilder.domain.clause.WithKey

/**
 * Estimates or counts exactly the cardinality of tag key values for the specified tag key on the current database unless a database is specified using the ON <database> option.
 */
class ShowTagValuesCardinality(
        val withKey: WithKey,
        val exact: Boolean = false,
        val on: On? = null,
        val from: From? = null,
        val where: Where? = null,
        val groupBy: GroupBy? = null,
        val limit: Limit? = null
) {
    override fun toString(): String {
        val sb = StringBuilder("SHOW TAG VALUES")

        if (exact) {
            sb.append(" EXACT")
        }

        sb.append(" CARDINALITY")

        on?.also {
            sb.append(" $it")
        }

        from?.also {
            sb.append(" $it")
        }

        where?.also {
            sb.append(" $it")
        }

        groupBy?.also {
            sb.append(" $it")
        }

        limit?.also {
            sb.append(" $it")
        }

        sb.append(" $withKey")

        return sb.toString()
    }
}
