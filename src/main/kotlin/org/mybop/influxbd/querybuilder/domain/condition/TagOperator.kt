package org.mybop.influxbd.querybuilder.domain.condition

enum class TagOperator(private val representation: String) {

    EQUAL_TO("="),
    NOT_EQUAL_TO("!=");

    override fun toString(): String = representation
}
