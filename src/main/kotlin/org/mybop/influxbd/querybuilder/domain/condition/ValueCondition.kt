package org.mybop.influxbd.querybuilder.domain.condition

import org.mybop.influxbd.querybuilder.domain.value.Value

class ValueCondition(
        val left: Value,
        val operator: ValueOperator,
        val right: Value
) : Condition {

    override fun toString(): String =
            "$left $operator $right"
}
