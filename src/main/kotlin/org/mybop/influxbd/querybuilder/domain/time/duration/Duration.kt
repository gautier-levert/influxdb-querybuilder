package org.mybop.influxbd.querybuilder.domain.time.duration

import org.mybop.influxbd.querybuilder.domain.time.TimeArithmeticOperator

interface Duration {

    fun add(duration: Duration) = CalculatedDuration(this, TimeArithmeticOperator.ADDITION, duration)

    fun subtract(duration: Duration) = CalculatedDuration(this, TimeArithmeticOperator.SUBTRACTION, duration)

    override fun toString(): String
}
