package org.mybop.influxbd.querybuilder.domain.condition

class JoinCondition(val operator: JoinOperator, private vararg val conditions: Condition) : Condition {

    override fun toString(): String =
            conditions.joinToString(" $operator ") {
                if (it is JoinCondition && it.operator != operator) {
                    "($it)"
                } else {
                    it.toString()
                }
            }
}
