package org.mybop.influxbd.querybuilder.domain.function.transformation

import org.mybop.influxbd.querybuilder.domain.function.Derivable
import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration

class Derivative(
        val field: Derivable,
        val unit: Duration? = null
) : Function {

    override fun toString() = unit?.let { "DERIVATIVE($field, $it)" } ?: "DERIVATIVE($field)"
}
