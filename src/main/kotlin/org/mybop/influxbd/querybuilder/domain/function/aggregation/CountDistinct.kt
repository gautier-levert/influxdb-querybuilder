package org.mybop.influxbd.querybuilder.domain.function.aggregation

import org.mybop.influxbd.querybuilder.domain.function.Function

class CountDistinct(
        val distinct: Distinct
) : Function {

    override fun toString() = "COUNT($distinct)"
}
