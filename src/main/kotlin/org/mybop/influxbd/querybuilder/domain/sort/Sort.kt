package org.mybop.influxbd.querybuilder.domain.sort

interface Sort {
    override fun toString(): String
}
