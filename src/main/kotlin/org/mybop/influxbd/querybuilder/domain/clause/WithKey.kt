package org.mybop.influxbd.querybuilder.domain.clause

interface WithKey {
    override fun toString(): String
}
