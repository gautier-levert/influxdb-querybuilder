package org.mybop.influxbd.querybuilder.domain.selection

import org.mybop.influxbd.querybuilder.domain.clause.Selectable

class Fields(
        field: Selectable,
        vararg others: Selectable
) : Selection {

    val fields = listOf(field, *others)

    override fun toString() = fields.joinToString(", ")

}
