package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.value.StringValue

/**
 * Stop currently-running query.
 */
class KillQuery(
        val queryId: Long,
        val host: StringValue? = null
) {

    override fun toString(): String {
        val sb = StringBuilder("KILL QUERY $queryId")

        host?.also {
            sb.append(" ON $it")
        }

        return sb.toString()
    }
}
