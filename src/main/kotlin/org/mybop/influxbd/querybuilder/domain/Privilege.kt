package org.mybop.influxbd.querybuilder.domain

enum class Privilege {
    ALL,
    READ,
    WRITE
}
