package org.mybop.influxbd.querybuilder.domain.selection

import org.mybop.influxbd.querybuilder.domain.clause.Aliasable
import org.mybop.influxbd.querybuilder.domain.clause.Selectable

class FieldWithAlias(
        val value: Aliasable,
        val alias: Alias? = null
) : Selectable {

    override fun toString(): String {
        val sb = StringBuilder("$value")

        alias?.also {
            sb.append(" $alias")
        }

        return sb.toString()
    }
}
