package org.mybop.influxbd.querybuilder.domain.function

import org.mybop.influxbd.querybuilder.domain.value.Value

interface Function : Value {

    override fun toString(): String
}
