package org.mybop.influxbd.querybuilder.domain.clause

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.selection.Alias
import org.mybop.influxbd.querybuilder.domain.selection.FieldWithAlias

interface Aliasable {

    infix fun alias(alias: Name) = FieldWithAlias(this, Alias(alias))

    infix fun alias(alias: String) = alias(Name(alias))

    override fun toString(): String
}
