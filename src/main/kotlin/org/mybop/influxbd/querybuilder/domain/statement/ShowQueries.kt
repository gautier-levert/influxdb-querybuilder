package org.mybop.influxbd.querybuilder.domain.statement

class ShowQueries {
    override fun toString() = "SHOW QUERIES"
}
