package org.mybop.influxbd.querybuilder.domain.function

interface FunctionArg {
    override fun toString(): String
}
