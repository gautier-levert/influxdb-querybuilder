package org.mybop.influxbd.querybuilder.domain.statement

class ShowShardGroups {
    override fun toString() = "SHOW SHARD GROUPS"
}
