package org.mybop.influxbd.querybuilder.domain.function.transformation

import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class Difference(
        val field: FunctionArg
) : Function {
    override fun toString() = "DIFFERENCE($field)"
}
