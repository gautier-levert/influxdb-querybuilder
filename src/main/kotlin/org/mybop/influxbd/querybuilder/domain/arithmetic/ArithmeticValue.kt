package org.mybop.influxbd.querybuilder.domain.arithmetic

import org.mybop.influxbd.querybuilder.domain.value.DoubleValue
import org.mybop.influxbd.querybuilder.domain.value.FloatValue
import org.mybop.influxbd.querybuilder.domain.value.IntValue
import org.mybop.influxbd.querybuilder.domain.value.LongValue
import org.mybop.influxbd.querybuilder.domain.value.Value

interface ArithmeticValue : Value {

    private fun operation(other: ArithmeticValue, operator: ArithmeticOperator) = ArithmeticOperation(this, other, operator)

    operator fun plus(other: ArithmeticValue) = operation(other, ArithmeticOperator.ADDITION)

    operator fun plus(other: Int) = operation(IntValue(other), ArithmeticOperator.ADDITION)

    operator fun plus(other: Long) = operation(LongValue(other), ArithmeticOperator.ADDITION)

    operator fun plus(other: Float) = operation(FloatValue(other), ArithmeticOperator.ADDITION)

    operator fun plus(other: Double) = operation(DoubleValue(other), ArithmeticOperator.ADDITION)

    operator fun minus(other: ArithmeticValue) = operation(other, ArithmeticOperator.SUBTRACTION)

    operator fun minus(other: Int) = operation(IntValue(other), ArithmeticOperator.SUBTRACTION)

    operator fun minus(other: Long) = operation(LongValue(other), ArithmeticOperator.SUBTRACTION)

    operator fun minus(other: Float) = operation(FloatValue(other), ArithmeticOperator.SUBTRACTION)

    operator fun minus(other: Double) = operation(DoubleValue(other), ArithmeticOperator.SUBTRACTION)

    operator fun times(other: ArithmeticValue) = operation(other, ArithmeticOperator.MULTIPLICATION)

    operator fun times(other: Int) = operation(IntValue(other), ArithmeticOperator.MULTIPLICATION)

    operator fun times(other: Long) = operation(LongValue(other), ArithmeticOperator.MULTIPLICATION)

    operator fun times(other: Float) = operation(FloatValue(other), ArithmeticOperator.MULTIPLICATION)

    operator fun times(other: Double) = operation(DoubleValue(other), ArithmeticOperator.MULTIPLICATION)

    operator fun div(other: ArithmeticValue) = operation(other, ArithmeticOperator.DIVISION)

    operator fun div(other: Int) = operation(IntValue(other), ArithmeticOperator.DIVISION)

    operator fun div(other: Long) = operation(LongValue(other), ArithmeticOperator.DIVISION)

    operator fun div(other: Float) = operation(FloatValue(other), ArithmeticOperator.DIVISION)

    operator fun div(other: Double) = operation(DoubleValue(other), ArithmeticOperator.DIVISION)

    override fun toString(): String
}
