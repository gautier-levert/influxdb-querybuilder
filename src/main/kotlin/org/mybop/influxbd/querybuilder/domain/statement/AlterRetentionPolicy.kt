package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On
import org.mybop.influxbd.querybuilder.domain.time.duration.Duration

class AlterRetentionPolicy(
        val policyName: Name,
        val on: On,
        val duration: Duration? = null,
        val replication: Int? = null,
        val shardDuration: Duration? = null,
        val default: Boolean = false
) {

    override fun toString(): String {
        val sb = StringBuilder("ALTER RETENTION POLICY $policyName $on")

        duration?.also {
            sb.append(" DURATION $it")
        }

        replication?.also {
            sb.append(" REPLICATION $it")
        }

        shardDuration?.also {
            sb.append(" SHARD DURATION $it")
        }

        if (default) {
            sb.append(" DEFAULT")
        }

        return sb.toString()
    }
}
