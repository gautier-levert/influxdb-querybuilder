package org.mybop.influxbd.querybuilder.domain.time.duration

object InfiniteDuration : Duration {
    override fun toString() = "INF"
}
