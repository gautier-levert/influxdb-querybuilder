package org.mybop.influxbd.querybuilder.domain.function.transformation

import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class NonNegativeDifference(
        val field: FunctionArg
) : Function {
    override fun toString() = "NON_NEGATIVE_DIFFERENCE($field)"
}
