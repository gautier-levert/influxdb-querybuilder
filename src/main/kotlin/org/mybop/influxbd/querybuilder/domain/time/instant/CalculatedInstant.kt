package org.mybop.influxbd.querybuilder.domain.time.instant

import org.mybop.influxbd.querybuilder.domain.time.duration.Duration
import org.mybop.influxbd.querybuilder.domain.time.TimeArithmeticOperator

class CalculatedInstant(
        private val instant: Instant,
        private val timeOperator: TimeArithmeticOperator,
        private val duration: Duration
) : Instant {

    override fun toString() = "$instant $timeOperator $duration"
}
