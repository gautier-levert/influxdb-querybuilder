package org.mybop.influxbd.querybuilder.domain.function.selector

import org.mybop.influxbd.querybuilder.domain.function.Derivable
import org.mybop.influxbd.querybuilder.domain.function.Function
import org.mybop.influxbd.querybuilder.domain.function.FunctionArg

class Percentile(
        private val key: FunctionArg,
        private val n: Int
) : Function, Derivable {
    override fun toString() = "PERCENTILE($key, $n)"
}
