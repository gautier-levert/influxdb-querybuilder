package org.mybop.influxbd.querybuilder.domain.time.instant

object Now : Instant {
    override fun toString() = "now()"
}
