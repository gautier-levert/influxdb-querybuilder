package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.clause.On

class DropRetentionPolicy(
        val policy: Name,
        val on: On
) {

    override fun toString() = "DROP RETENTION POLICY $policy $on"
}
