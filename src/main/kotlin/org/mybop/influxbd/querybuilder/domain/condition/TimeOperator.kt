package org.mybop.influxbd.querybuilder.domain.condition

enum class TimeOperator(private val representation: String) {
    AFTER(">="),
    STRICTLY_AFTER(">"),
    BEFORE("<="),
    STRICTLY_BEFORE("<");

    override fun toString() = representation
}
