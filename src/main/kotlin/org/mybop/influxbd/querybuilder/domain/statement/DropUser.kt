package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.Name

class DropUser(
        val user: Name
) {

    override fun toString(): String {
        return "DROP USER $user"
    }
}
