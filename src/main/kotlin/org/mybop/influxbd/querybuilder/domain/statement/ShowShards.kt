package org.mybop.influxbd.querybuilder.domain.statement

class ShowShards {
    override fun toString() = "SHOW SHARDS"
}
