package org.mybop.influxbd.querybuilder.domain.statement

import org.mybop.influxbd.querybuilder.domain.DestinationMode
import org.mybop.influxbd.querybuilder.domain.Name
import org.mybop.influxbd.querybuilder.domain.value.StringValue

class CreateSubscription(
        val subscriptionName: Name,
        val database: Name,
        val retentionPolicy: Name,
        val mode: DestinationMode,
        host: StringValue,
        vararg others: StringValue
) {
    val hosts = listOf(host, *others)

    override fun toString() = "CREATE SUBSCRIPTION $subscriptionName ON $database.$retentionPolicy DESTINATIONS $mode ${hosts.joinToString(", ")}"
}
